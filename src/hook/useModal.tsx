import { useState } from 'react'

const useModal = () => {
    const [isOpen, setIsOpen] = useState(false);
        const togle = () =>{
            setIsOpen(!isOpen)
        }
  return {isOpen, togle}
}

export default useModal