import './styles/App.css'
import Navbar from './components/Navbar'
import Modal from './components/Modal'
import useModal from './hook/useModal'
import UserAccount, { User } from './components/UserAccount'
import TaskList from './components/TaskList'

function App() {
  const {isOpen, togle} = useModal()
  const user : User = new UserAccount('Iliyan', 21342512)
  
  return (
    <div className='App'>
      <Navbar/>
      {/* <Contact togle ={togle}/> */}
      {/* <Generics>Generic arrays string and numbers</Generics> */}
      <TaskList/>
      <Modal isOpen={isOpen} togle={togle}>
          <h3>name: {user.name}</h3>
          <p>id: {user.id}</p>
      </Modal>
    </div>
  )
}

export default App
