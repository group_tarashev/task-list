import React, { ReactNode } from "react";
import '../styles/modal.css'
import '../styles/tasklist.css'
type ModalType = {
  children?: ReactNode;
  isOpen: boolean;
  togle: (id: string) => void;
}

const Modal: React.FC<ModalType> = ({isOpen, togle, children}) => {
  return (
    <>
    {isOpen &&( <div className="modal-overlay">
      <div className="modal">
        <button className="btn-modal" onClick={() =>togle("")}>Close X</button>
        {children}
      </div>
    </div>)}
    </>
  );
};

export default Modal;
