import {JSX} from 'react'
import '../styles/contact.css'
import {SlBubble} from "react-icons/sl"


const Contact = ({togle} : any) : JSX.Element => {
  return (
    <button className='contact' onClick={togle}>
        <SlBubble size={30}/>
    </button>
  )
}

export default Contact