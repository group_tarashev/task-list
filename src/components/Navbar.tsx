import '../styles/navbar.css'

export default function Navbar() :JSX.Element {
  return (
    <div className='navbar'>
        <div className="logo">
            <img src="https://prolog.profy.dev/icons/logo-large.svg" alt="" />
        </div>
        <div className="dashboard">
          <button className='btn-dash'>Dashboard</button>
        </div>
        
    </div>
  )
}
