import React from "react";
import "../styles/tasklist.css";

type FormProps = {
  handleSubmit: (e: any, prior: string, task: string) => void;
  updateSubmit: (e: any, prior: string, task: string) => void;
  btn: string;
  task: string;
  prior: string;
  handleTask: (e: string) => void;
  handlePrior: (e: string) => void;
};

const Form: React.FC<FormProps> = ({
  handleSubmit,
  updateSubmit,
  btn,
  prior,
  task,
  handlePrior,
  handleTask,
}) => {
  // handlePrior('');
  // handleTask('');
  return (
    <div className="task-outer">
      <form className="addtask">
        <div className="top-task">
          <h2>Add Taks</h2>
        </div>
        <div className="input-task">
          <h3 className="task-lable">Task</h3>
          <input
            type="text"
            name=""
            id=""
            placeholder="send article to editor"
            onChange={(e) => handleTask(e.target.value)}
            value={task}
          />
        </div>
        <div className="priority">
          <h3 className="task-priority">Priority</h3>
          <div className="pr-btn-group">
            <button
              className="btn-high"
              type="button"
              onClick={() => handlePrior("High")}
            >
              High
            </button>
            <button
              className="btn-med"
              type="button"
              onClick={() => handlePrior("Medium")}
            >
              Medium
            </button>
            <button
              className="btn-low"
              type="button"
              onClick={() => handlePrior("Low")}
            >
              Low
            </button>
          </div>
        </div>
        {btn === "Edit" && (
          <button
            className="btn-add-task"
            type="submit"
            onClick={(e) => updateSubmit(e, prior, task)}
          >
            {btn}
          </button>
        )}
        {btn === "Add Task" && (
          <button
            className="btn-add-task"
            type="submit"
            onClick={(e) => handleSubmit(e, prior, task)}
          >
            {btn}
          </button>
        )}
      </form>
    </div>
  );
};

export default Form;
