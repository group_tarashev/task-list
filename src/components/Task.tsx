import React, { useEffect, useState } from "react";
import "../styles/tasklist.css";
import { RiDeleteBin2Line } from "react-icons/ri";
import { BiEditAlt } from "react-icons/bi";
import { TaskItem } from "../types/type";
// import { TaskItem, initialstate } from "./TaskList";

type Tasks = {
  name: string;
  priority: string;
  id: string;
  filt: (name: string) => void;
  // update: (id: string, newName : string, newPriority: string) => void
  arr: TaskItem[];
  // handleEdit: (id: string, tasl: string, prior: string) => void
  toggleEdit: (id: string) => void;
  toggleProgress: (id: string, change: boolean) => void;
  index: number;
};

const Task: React.FC<Tasks> = ({
  name,
  priority,
  id,
  filt,
  toggleEdit,
  toggleProgress,
  arr,
  index,
}) => {
  const [change, setChange] = useState(false);
  const [color, setColor] = useState("green");
  const [circle, setCircle] = useState(10);
  const findInd = arr.findIndex((curr) => curr.id === id);
  const prog = arr[findInd].progress;
  const handleChange = () => {
    toggleProgress(id, change);
    setChange(!change);
    if (prog.toLowerCase() === "in progress") {
      setCircle(25);
    } else {
      setCircle(10);
    }
  };

  useEffect(() => {
    if (priority.toLowerCase() == "low") {
      setColor("green");
    } else if (priority.toLowerCase() === "medium") {
      setColor("orange");
    } else {
      setColor("red");
    }
  }, []);
  return (
    <div className="task-inner">
      <div className="task-in">
        <p>Task</p>
        <h4>{name}</h4>
      </div>
      <div className="prior-in">
        <p>Priority</p>
        <h4 style={{ color: color }}>{priority}</h4>
      </div>
      <div className="progress">
        <p>Progress</p>
        <ul className="ul-progres" onClick={() => handleChange()}>
          <li className={prog === 'In Progress' ? "animate-progress" : ""}>{arr[index].progress}</li>
        </ul>
        <span style={{ width: `${circle}%` }} className="circle"></span>
      </div>
      <div className="icons">
        <BiEditAlt size={30} onClick={() => toggleEdit(id)} />
        <RiDeleteBin2Line onClick={() => filt(id)} color={"red"} size={30} />
      </div>
    </div>
  );
};

export default Task;
