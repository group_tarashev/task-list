
export interface User{
    name: string,
    id: number
}

export default class UserAccount {
  name : string
  id: number
  constructor(name: string, id: number){
    this.name = name,
    this.id = id
  }
}
