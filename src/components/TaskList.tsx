import { useEffect, useState } from "react";
import useModal from "../hook/useModal";
import Modal from "./Modal";
import "../styles/tasklist.css";
import Task from "./Task";
import Form from "./Form";
import { TaskItem } from "../types/type";

export const initialstate: TaskItem[] = [];

const TaskList = () => {
  const { isOpen, togle } = useModal();
  const [arr, setArr] = useState<TaskItem[]>([]);
  const [edit, setEdit] = useState(false);
  const [id, setId] = useState("");
  const [task, setTask] = useState("");
  const [prior, setPrior] = useState("");
  const ls  = localStorage.getItem("arrayTasks");

  const handleStorage = async () =>{
    if(arr.length !== 0)localStorage.setItem('arrayTasks', JSON.stringify(arr))
  }
  useEffect(()=>{
    if (ls) {
      const jsLs = JSON.parse(ls);
      setArr(jsLs)
      if(jsLs.length <= 2){
        localStorage.setItem("arrayTasks", JSON.stringify(arr));
      }else{
        setArr(jsLs)
      }
    }
  },[])

  const handleSubmit = (e: any, prior: string, task: string) => {
    e.preventDefault();
    handleTask("");
    handlePrior("");
    if (prior.length === 0) {
      alert("Pick priority");
      return;
    } else if (task.length === 0) {
      alert("Pick taks");
      return;
    }

    setArr((setPrev) => [
      ...setPrev,
      {
        id: Date.now().toString(),
        name: task,
        priority: prior,
        progress: 'In Progress'
      },
    ]);
    // setUpdate(!update)
    handleStorage();
    setTimeout(() => {
      togle();
    }, 500);
  };

  const updateSubmit = (e: any, prior: string, task: string) => {
    e.preventDefault();
    setArr((setPrev) => {
      const newArr = [...setPrev];
      const index = newArr.findIndex((currentTask) => currentTask.id === id);
      newArr[index].name = task;
      newArr[index].priority = prior;
      return newArr;
    });

    toggleEdit(id);
  };
  
  const handleTask = (e: string) => {
    setTask(e);
  };
  const handlePrior = (e: string) => {
    setPrior(e);
  };

  const filt = (id: string) => {
    const fitlArr = arr.filter((item) => item.id !== id);
    setArr(fitlArr);
  };

  const toggleEdit = (id: string) => {
    const index = arr?.findIndex((curr) => curr.id === id);
    handleTask(arr[index]?.name.toString());
    handlePrior(arr[index]?.priority.toString());
    setEdit(!edit);
    setId(id);
  };
  const toggleProgress = (id: string, change: boolean) =>{
    const index = arr?.findIndex((curr) => curr.id === id)
    setArr((prevArr) =>{
      const newArr = [...prevArr]
      newArr[index].progress = change ? "Done" : "In Progress"
      return newArr
    })
  }
  useEffect(()=>{
    localStorage.setItem("arrayTasks", JSON.stringify(arr))
  },[arr])


  return (
    <div className="tasklist">
       <p className="info-text">
          Tasks are stored in LocalStorage in your Browser
       </p>
      <div className="nav-task">
        <h2>Task List</h2>
        <button className="btn-add" onClick={togle}>
          + Add Task
        </button>
      </div>
      {/* Register Task Modal ----------------------- */}
      {isOpen && (
        <Modal isOpen={isOpen} togle={togle}>
          <Form
            handleSubmit={handleSubmit}
            updateSubmit={updateSubmit}
            btn={"Add Task"}
            prior={prior}
            task={task}
            handleTask={handleTask}
            handlePrior={handlePrior}
          />
        </Modal>
      )}
      {/* Edit form modal --------------------------- */}
      {edit && (
        <Modal isOpen={edit} togle={toggleEdit}>
          <Form
            updateSubmit={updateSubmit}
            handleSubmit={handleSubmit}
            btn={"Edit"}
            prior={prior}
            task={task}
            handleTask={handleTask}
            handlePrior={handlePrior}
          />
        </Modal>
      )}
      {/* Show ALl tasks ---------------------------- */}
      {arr.length > 0 &&
        arr.map((item, index) => (
          <Task
            index={index}
            name={item.name}
            priority={item.priority}
            key={item.id}
            id={item.id}
            filt={filt}
            arr={arr}
            toggleEdit={toggleEdit}
            toggleProgress={toggleProgress}
          />
        ))}
        {arr.length > 1 && <div className="clear">
          <button className="btn-clear" onClick={() => setArr([])}>Clear All Task</button>
        </div>}
    </div>
  );
};

export default TaskList;
