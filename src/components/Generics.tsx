import  { ReactNode } from 'react'
import '../styles/generics.css'
interface genTypes {
    children?: ReactNode
}
// interface Point {
//     x: number,
//     y: number
// }

type StringArray = Array<string>
type NumberArray = Array<number>

  const arrSt: StringArray = [
    "Name", "Id", "Street", "City", "Car"
  ]
  const arrNm: NumberArray = [
    1,3,5,7,9
  ]

// function logPoint(){
    // console.log(p.x, " ", p.y);
    
// }

// const point = {x: 12, y:21}
// logPoint(point);
const Generics = (prop :genTypes) => {
  return (
    <div className='generics'>
        {prop.children}
        {arrSt.map((item, i) =>(
            <p key={i}>{item}</p>
        ))}
        <hr />
        {arrNm.map((item,i) =>(
            <p key={i}>{item}</p>
        ))}
    </div>
  )
}

export default Generics