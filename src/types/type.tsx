export type TaskItem = {
    id: string;
    name: string;
    priority: string;
    progress: string
  };

  // type arrStateFunction = (prevValue: TaskItem[]) => TaskItem[];

 